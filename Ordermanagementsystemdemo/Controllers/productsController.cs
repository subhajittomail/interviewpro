﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Ordermanagementsystemdemo;
using System.IO;
using Ordermanagementsystemdemo.Models;

namespace Ordermanagementsystemdemo.Controllers
{
    public class productsController : Controller
    {
        
        private OrdermanagmentsystemDBEntities db = new OrdermanagmentsystemDBEntities();

        // GET: products
        public ActionResult Index()
        {
            string strUseraname = Session["name"].ToString();
            var roleid = db.users.Where(x => x.username == strUseraname).Select(x => x.roleid).FirstOrDefault();
            ViewBag.roleid = roleid;

            return View(db.products.ToList());
        }

        // GET: products/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            product product = db.products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // GET: products/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: products/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create( ProductModel product)
        {
            

            string FolderPath = Server.MapPath("~/images/");
            string _filename = DateTime.Now.ToString("yymmssfff") + Path.GetFileNameWithoutExtension(product.Imagepath.FileName);
            product p = new product
            {
                name = product.name,
                image = "/images/" + _filename,
                availablequentity = product.availablequentity,
                price=product.price
            };


            if (ModelState.IsValid)
            {
                
                string extension = Path.GetExtension(product.Imagepath.FileName);
                
                
                if (extension.ToLower() == ".jpg" || extension.ToLower() == ".png" || extension.ToLower() == ".jpeg")
                {
                    if (product.Imagepath.ContentLength <= 3000000)
                    {
                        db.products.Add(p);
                        if (db.SaveChanges() > 0)
                        {
                            product.Imagepath.SaveAs(FolderPath + _filename);
                            ViewBag.msg = "product saved";
                        }
                        
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        ViewBag.msg = "file size not valid";
                    }
                }
            }


            return View(product);
        }
        public ActionResult Addtocart(int Id)
        {
            product pro = db.products.Where(x => x.id == Id).SingleOrDefault();
            return View(pro);

            
            
        }

        List<Cart> li = new List<Cart>();

        [HttpPost]
        public ActionResult Addtocart(product pro,string qty, int id)
        {

            product p = db.products.Where(x => x.id == id).SingleOrDefault();

           Cart c = new Cart();
   
            string strUseraname = Session["name"].ToString();
            c.productid = pro.id;
            c.customername = db.users.Where(x => x.username == strUseraname).Select(x => x.name).FirstOrDefault();
            c.productid = p.id;
            c.orderstatus = "Inprogress";
            c.price = pro.price;
            c.address = db.users.Where(x => x.username == strUseraname).Select(x => x.address).FirstOrDefault();
            
            //db.orders.Add(ordr);
            //db.SaveChanges();
            if (TempData["product"] == null)
            {
                li.Add(c);
                TempData["product"] = li;

            }
            else
            {
                List<Cart> li2 = TempData["product"] as List<Cart>;

                li2.Add(c);
                TempData["product"] = li2;
            }
            TempData.Keep();
            ViewBag.customername = c.customername;
            return RedirectToAction("Index"/*p1*/);
        }

        public ActionResult Checkout()
        {
            List<Cart> cart = TempData["product"] as List<Cart>;
            TempData.Keep();
            return View();
        }
        [HttpPost]
        public ActionResult Checkout(order odr)
        {
            string strUseraname = Session["name"].ToString();
            List<Cart> cart = TempData["product"] as List<Cart>;
            foreach (var item in cart)
            {
                
                odr.customername = item.customername;
                odr.customerid = db.users.Where(x => x.username == strUseraname).Select(x => x.id).FirstOrDefault();
                odr.orderstatus = "inprogress";
                odr.address = db.users.Where(x => x.username == strUseraname).Select(x => x.address).FirstOrDefault();
                odr.productID = item.productid;
                db.orders.Add(odr);
                db.SaveChanges();
            }
            TempData.Remove("product");
            TempData["msg"] = "Oder has been done";
            TempData.Keep();
            return RedirectToAction("Index");
        }

        // GET: products/Edit/5
        public ActionResult Edit(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            product product = db.products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // POST: products/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit( product product)
        {
            if (ModelState.IsValid)
            {
                db.Entry(product).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(product);
        }

        // GET: products/Delete/5
        public ActionResult Delete(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            product product = db.products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // POST: products/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            product product = db.products.Find(id);
            db.products.Remove(product);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        
    }
}
