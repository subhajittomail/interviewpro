﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Ordermanagementsystemdemo.Controllers
{
    public class OrderController : Controller
    {
        private OrdermanagmentsystemDBEntities db = new OrdermanagmentsystemDBEntities();
        // GET: Order
        public ActionResult Index()
        {
            OrdermanagmentsystemDBEntities db = new OrdermanagmentsystemDBEntities();
            string strUseraname = Session["name"].ToString();
            var  roleid = db.users.Where(x => x.username == strUseraname).Select(x=> x.roleid ) .FirstOrDefault ();
            ViewBag.Roleid = roleid;
            return View(db.orders.ToList());
        }

        public ActionResult Orderbyperperson()
        {
            string strUseraname = Session["name"].ToString();
            var id = db.users.Where(x => x.username == strUseraname).Select(x => x.id).FirstOrDefault();
            var data = db.sp_Getoderbyid(id);
            return View(data);
        }
        
        //for mail send purpose 
        public JsonResult Sendmailtouser()
        {    
            bool result = false;
            string strUseraname = Session["name"].ToString();
            var usermailid = db.users.Where(x => x.username == strUseraname).Select(x => x.email).FirstOrDefault();
            result = sendmail(usermailid, "Navtech project","order has been placed");
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public bool sendmail(string tomail,string subject,string body)
        {
            try
            {
                string sendermail = System.Configuration.ConfigurationManager.AppSettings["Sendermail"].ToString();
                string senderpassword = System.Configuration.ConfigurationManager.AppSettings["Senderpassword"].ToString();

                SmtpClient cl = new SmtpClient("smtp.gmail.com",587);
                cl.EnableSsl = true;
                cl.Timeout = 100000;
                cl.DeliveryMethod = SmtpDeliveryMethod.Network;
                cl.UseDefaultCredentials = false;
                cl.Credentials = new NetworkCredential(sendermail, senderpassword);
                MailMessage msg = new MailMessage(sendermail, tomail, subject, body);
                msg.IsBodyHtml = true;
                msg.BodyEncoding = UTF8Encoding.UTF8;
                cl.Send(msg);
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }
    }
}