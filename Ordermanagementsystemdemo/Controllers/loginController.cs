﻿using Ordermanagementsystemdemo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Ordermanagementsystemdemo.Controllers
{
    public class loginController : Controller
    {
        // GET: login
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(user obj)
        {
              OrdermanagmentsystemDBEntities db = new OrdermanagmentsystemDBEntities();
            var data = db.sp_logindetails(obj.username, obj.password);
            foreach(var item in data)
            {
                if(item.Username==obj.username && item.Password == obj.password)
                {
                    Session["name"] = obj.username;
                    return RedirectToAction("main");
                }
                else { }
                
            }
            return View();
        }
        public ActionResult Main()
        {
            if (Session["name"] == null)
            {
                return RedirectToAction("Index", "loginController");
            }
            else { return View(); }
        }
        public ActionResult _menu()
        {
            OrdermanagmentsystemDBEntities db = new OrdermanagmentsystemDBEntities();
            string strUseraname = Session["name"].ToString();
            var model = db.users.Where(x => x.username == strUseraname).FirstOrDefault();
           // var model = db.users.ToList();
            return PartialView("_menu", model);
        }

    }
}