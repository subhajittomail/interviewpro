﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Ordermanagementsystemdemo.Startup))]
namespace Ordermanagementsystemdemo
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
