﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ordermanagementsystemdemo.Models
{
    public class Cart
    {

        public int productid { get; set; }

        public string productname { get; set; }

        public string customername { get; set; }
        public int customerid { get; set; }

        public int qty { get; set; }
        public int price { get; set; }

        public string orderstatus { get; set; }
        public string address { get; set; }


    }
}