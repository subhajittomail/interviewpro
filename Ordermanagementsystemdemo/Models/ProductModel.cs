﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace Ordermanagementsystemdemo.Models
{
    public class ProductModel
    {
        public int id { get; set; }
        public string name { get; set; }
        public int price { get; set; }
        [DisplayName("Upload Image")]
        public string image { get; set; }

        public int availablequentity { get; set; }
        public HttpPostedFileBase Imagepath { get; set; }
    }
}