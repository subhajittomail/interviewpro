﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ordermanagementsystemdemo.Models
{
    public class UserModel
    {
        public int id { get; set; }
        public string name { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string phonenumber { get; set; }
        public string address { get; set; }
        public byte roleid { get; set; }
        public string email { get; set; }


    }
}